var wall_proxy = "PROXY  127.0.0.1:8118;";
var nowall_proxy = "DIRECT;";
var direct = "DIRECT;";
var ip_proxy = "DIRECT;";

/*
 * Copyright (C) 2014 breakwa11
 * https://github.com/breakwa11/gfw_whitelist
 */

var black_domains = {"am":{

},"biz":{
},"org":{
"openwrt":1,
"mathjax":1,
"wikipedia":1,
"mozilla":1
},"com":{
"wordpress":1,
"appspot":1,
"sowingseasons":1,
"host1plus":1,
"blogger":1,
"blogblog":1,
"blogspot":1,
"dropbox":1,
"yjbys":1,
"html5rocks":1,
"gmail":1,
"amazonaws":1,
"w3schools":1,
"googletagservices":1,
"googlevideo":1,
"googleapis":1,
"twitter":1,
"twimg":1,
"google-analytics":1,
"github":1,
"googleusercontent":1,
"googleadservices":1,
"ytimg":1,
"ggpht":1,
"youtube":1,
"google":1,
"gstatic":1
},"cl":{
"google":1
},"io":{
"draw":1,
"qt":1,
"github":1
},"net":{
"sourceforge":1
},"be":{
"youtu":1
},"co":{
"google":1
},"hk":{
"google":1
}
};




var subnetIpRangeList = [
0,1,
167772160,184549376,	//10.0.0.0/8
2886729728,2887778304,	//172.16.0.0/12
3232235520,3232301056,	//192.168.0.0/16
2130706432,2130706688	//127.0.0.0/24
];

var hasOwnProperty = Object.hasOwnProperty;

function check_ipv4(host) {
	// check if the ipv4 format (TODO: ipv6)
	//   http://home.deds.nl/~aeron/regex/
	var re_ipv4 = /^\d+\.\d+\.\d+\.\d+$/g;
	if (re_ipv4.test(host)) {
		// in theory, we can add chnroutes test here.
		// but that is probably too much an overkill.
		return true;
	}
}
function convertAddress(ipchars) {
	var bytes = ipchars.split('.');
	var result = (bytes[0] << 24) |
	(bytes[1] << 16) |
	(bytes[2] << 8) |
	(bytes[3]);
	return result >>> 0;
}
function isInSubnetRange(ipRange, intIp) {
	for ( var i = 0; i < 10; i += 2 ) {
		if ( ipRange[i] <= intIp && intIp < ipRange[i+1] )
			return true;
	}
}
function getProxyFromDirectIP(strIp) {
	var intIp = convertAddress(strIp);
	if ( isInSubnetRange(subnetIpRangeList, intIp) ) {
		return direct;
	}
	return ip_proxy;
}
function isInDomains(domain_dict, host) {
	var suffix;
	var pos1 = host.lastIndexOf('.');

	suffix = host.substring(pos1 + 1);
	if (suffix == "cn") {
		return true;
	}

	var domains = domain_dict[suffix];
	if ( domains === undefined ) {
		return false;
	}
	host = host.substring(0, pos1);
	var pos = host.lastIndexOf('.');

	while(1) {
		if (pos <= 0) {
			if (hasOwnProperty.call(domains, host)) {
				return true;
			} else {
				return false;
			}
		}
		suffix = host.substring(pos + 1);
		if (hasOwnProperty.call(domains, suffix)) {
			return true;
		}
		host=host.substring(0, pos);
		pos = host.lastIndexOf('.', pos - 1);
	}
}
function FindProxyForURL(url, host) {
	if ( isPlainHostName(host) === true ) {
		//alert("direct:"+host);
		return direct;
	}
	if ( check_ipv4(host) === true ) {
		return getProxyFromDirectIP(host);
	}
	//if ( isInDomains(white_domains, host) === true ) {
	if ( isInDomains(black_domains, host) === true ) {
	//alert("wall_proxy:"+host);
		return wall_proxy;
	}
	//alert("nowall_proxy:"+host);
	return nowall_proxy;
}

